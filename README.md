La idea es realizar un proyecto en Angular que consuma nuestros componentes reusable, API de datos externos.

La premisa es la adaptación al uso de los componentes existen, proponer mejoras y buenas prácticas desde el inicio, evaluar el uso de node, NPM, Scrum, GIT, branching y versioning del proyecto. Así mismo, seguir una historia funcional para nuevos features del proyecto.

## Requerimientos mínimos

Crear una nueva vista según la historia, usando los componentes existentes, adaptando hojas de estilos.
1. Hacer un fork de este repositorio, 
2. Crear una vista para las licencias administrativas
3. Utilizar los componentes en sharerd/reusable (Ver DOCUMENTATION.md)
4. Consumir los datos del API mockeada
5. (opcional) Se valoran y suman puntos si se agregan vistas de detalles
6. (opcional) Agregar o crear nuevos componentes reusables para mostrar información, sin importar la complejidad del componente, por ejemplo: Los estados “Ingresada”, “Autorizada”, “Rechazada” o “Documentación de Respaldo Pendiente”.
7. (opcional) Se valora pruebas unitarias
8. (opcional) Hacer un pull request o merge request a este repositorio
9. Enviar por email, el link del repositorio público y/o link del PR (o MR)


## Recursos:
- Data Mock: https://api.npoint.io/62d5511c3ebc74a3875a o https://api.jsonbin.io/b/5f0887eb5d4af74b0129dd77 (Actualizado: 17/07/2020 12:45)
- Angular 8+
- Angular CLI
- Node +8
- NPM +6
- ng-boostrap (Angular Boostrap)

SECRET_KEY:  ```SE ENVIA VIA MAIL```

Ejemplo (Actualizado: 17/07/2020 12:45): 
```
curl https://api.npoint.io/62d5511c3ebc74a3875a
curl --header "secret-key:<SECRET_KEY>" --request GET https://api.jsonbin.io/b/5f0887eb5d4af74b0129dd77
```  

## Estimado
Duración: 3 días   
Sprint: 17   
Story point: 3  


## Historia funcional

**DESCRIPCIÓN**  
Como Autorizante, quiero poder visualizar las licencias administrativas de los empleados de mi unidad organizativa, para poder administrarlas.

**OBJETIVOS**  
El Autorizante necesita visualizar todas las licencias administrativas realizadas por los empleados de su Unidad Organizativa que tiene a su cargo, pudiendo ver sus datos y aprobar o rechazar las que se encuentren pendientes de autorización.

**CRITERIOS DE ACEPTACIÓN**

-   Por cada solicitud, se deben mostrar las columnas:
    -   **Id solicitud:** es el identificador único de la solicitud.
    -   **Nombre:** Es el nombre del empleado que solicitó la licencia.
    -   **Apellido:** Es el apellido del empleado que solicitó la licencia.
    -   **ID HR**: Es el ID del empleado en META4.       
    -   **CUIL:** Es el número de CUIL del empleado que solicitó la licencia.
    -   **Licencia:** Es la licencia con el que se creó la solicitud
    -   **Unidad Organizativa:** Es la unidad organizativa correspondiente de entre las seleccionadas en la carga de la solicitud.
    -   **Cargo**: Es el cargo en la unidad organizativa.
    -   **Fecha de Inicio:** Fecha de Inicio de la Licencia        
    -   **Fecha de Fin:** Fecha de Fin de la Licencia
    -   **Estado:** Se debe mostrar las licencias administrativas con estado distinta a “Anulada“.
    -   (opcional) La aplicación debe listar las solicitudes de licencia administrativa en estados “Ingresada”, “Autorizada”, “Rechazada” o “Documentación de Respaldo Pendiente”.
    -   (opcional) La aplicación debe permitir las siguientes acciones para las solicitudes de licencia administrativas listadas para un Autorizante:
        -   Ver Detalle 
    



 
  
   
Actualizado: 13/07/2020 por [Daniel Naranjo](https://twitter.com/naranjodaniel)  






RECUERDE: Enviar el link público del repositorio para su revisión.