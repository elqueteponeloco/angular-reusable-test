import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { backofficeRoutes } from './backoffice.routes';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { DashboardComponent } from './dashboard/dashboard.component';

@NgModule({
	declarations: [
	DashboardComponent],
	imports: [
		CommonModule,
		RouterModule.forChild(backofficeRoutes),
		SharedModule,
	],
	exports: [
	DashboardComponent],
	providers: [
	],
	entryComponents: [
	],
})
export class BackofficeModule { }
