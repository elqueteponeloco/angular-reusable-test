import { Routes } from '@angular/router';
import { AdministrativeComponent } from './administrative/administrative.component';
import { DashboardComponent } from './dashboard/dashboard.component';

export const backofficeRoutes: Routes = [
	{
		path: '',
		component: DashboardComponent,
		children: [
			{
				path: 'administrative',
				component: AdministrativeComponent,
			}
		]
	},
];
