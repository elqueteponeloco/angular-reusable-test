import { Routes } from '@angular/router';

export const rootRoutes: Routes = [
	{
		path: '',
		pathMatch: 'full',
		redirectTo: 'login'
	},
	{
		path: 'login',
		loadChildren: './login/login.module#LoginModule',
	},
	{
		path: 'license',
		loadChildren: './license/license.module#LicenseModule',
	},
	{
		path: 'backoffice',
		loadChildren: './backoffice/backoffice.module#BackofficeModule',
	},
	{
		path: 'onBoarding/:cuil/:role',
		loadChildren: './onBoarding/onBoarding.module#OnBoardingModule'
	},
	{
		path: 'token',
		loadChildren: './token/token.module#TokenModule',
	}
];
