import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AdministrativeComponent } from './backoffice/administrative/administrative.component';
import { ListsComponent } from './shared/reusable/lists/lists.component';

@NgModule({
  declarations: [
    AppComponent,
    AdministrativeComponent,
    ListsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [AdministrativeComponent, ListsComponent]
})
export class AppModule { }
